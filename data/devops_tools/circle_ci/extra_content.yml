pages:
  - path: bdm
    content:
      page_title: CircleCI vs. Gitlab for Business Decision Makers
      css: devops-tools/extra-content.css
      page_body: |
        ## CircleCI Strengths

        * YAML File:  CI build jobs are configured in a YAML file
        * Security Accreditations: CircleCI is FedRamp authorized and SOC 2 compliant
        * Forrester Wave: CircleCI was recognized as a Leader in The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019: speed, scale, security, and compliance.
        * Catering to Large Enterprise:
          - CircleCI provides a large number of preconfigured environments, which is highly favored by enterprises
          - Despite a team size of just over 300 employees, CircleCI has a Customer Success Team and Enterprise support packages, also favored by enterprises
        * iOS application testing on macOS: CircleCI offers support for building and testing iOS projects in macOS virtual machines (available on CircleCI Cloud only, not currently available on self-hosted installations).
          - GitLab is actively working on integrating this functionality, more details can be found [here](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/5720)

        ## CircleCI Limitations and Challenges


        * Free Plan Trap: Their free plan offers a generous amount of free credits, 2,500/week.  However, these credits are used to pay for CI run times across medium sized Linux and Windows machines only; and are used for Orb usage, Workspaces and Dependency Caching.
        * No Single Integrated DevOps Application: CircleCI is a tool that automates the Continuous Integration stage of the Software Development Life Cycle.  To extend the functionality beyond CI, integration with third party plugins is required.  Plugins are expensive to maintain, secure, and upgrade. In contrast, GitLab is [open core](https://about.gitlab.com/blog/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.
        * Missing Enterprise Features: CircleCI lacks native support for key enterprise features such as Feature Flags, Kubernetes Support and Canary Deployments.
        * Hybride CI: CircleCI lacks the ability to orchestrate a customers private server CI builds with their cloud hosted CI server

        ## GitLab Strengths over CircleCI

        |         GitLab Differentiator        |                                                                                                                                                                                                                Why Is This Important?                                                                                                                                                                                                                |
        |:------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
        | Intuitive and Customizable Dashboard | CircleCI customers have reported to Forrester analyst the desire to have a more customizable UI experience when using CircleCI                                                                                                                                                                                                                                                                                                                       |
        | Fully Integrated CI/CD               | Fully integrated, means less context switching between tools and more time delivering software.  CircleCI is a CI tool that uses plugins for CD and other functions                                                                                                                                                                                                                                                                                  |
        | Integrated Container Registry        | Fully integrated container registry offers more security, governance and efficient management.  CircleCI does not offer this                                                                                                                                                                                                                                                                                                                         |
        | Local Artifact Storage               | Fully integrated artifact storage  offers more security, governance and efficient management.  CircleCI does not offer this, they store Artifacts on Amazon S3.                                                                                                                                                                                                                                                                                      |
        | Rich Insights and Analytics          | Detailed analytics and insights helps customers improve software development times.  Aggregating insights and analytics across projects provides users with easy access to valuable information.  GitLab does this well while CircleCI does not.  CircleCI’s on-prem CI server offers very shallow insights.  Their SaaS CI solution provides deeper insights but requires a separate self-managed server and is not integrated into their Cloud UI. |
        | Hybrid CI Model                      | More and more customers are attracted to SaaS tools but don’t want to place their source code in the cloud.  For this reason Hybrid CI solutions that combine a cloud-native SaaS CI server with on-premises agents are becoming more desirable because it simplifies tool maintenance while dodging security concerns.                                                                                                                              |
        | Single Application                   | Built-in CI/CD transforms how you deliver today and simultaneously prepares your SDLC for evolving needs tomorrow.                                                                                                                                                                                                                                                                                                                                   |
        | End-to-End Automation                | Powerful enough alone yet flexible enough to integrate with other tools, GitLab ensures quality code gets to production faster.                                                                                                                                                                                                                                                                                                                      |

        ## CircleCI vs GitLab Summary

        |  GitLab |                                                                            |  CircleCI |
        |:-------:|:--------------------------------------------------------------------------:|:---------:|
        | YES     |                            Self Hosted and .com                            | YES       |
        | YES     |               Ecosystem (https://about.gitlab.com/partners/)               | YES       |
        | No      |                                 Marketplace                                | YES       |
        | YES     |            CD fully integrated - no 3rd party Plugins/tools need           | No        |
        | YES     |                Built in Kubernetes Deployment and Monitoring               | No        |
        | YES     |                       Auto CI  Pipeline Configuration                      | No        |
        | YES     |      Built in CI Security Scanning - no 3rd party Plugins/tools needed     | No        |
        | YES     |           Security Dashboard enabling Security Team collaboration          | No        |
        | YES     | Supports Hybrid CI Orchestration Model (SaaS CI Server  and On-Prem Agent) | No        |

  - path: license
    content:
      page_title: CircleCI License Overview
      css: devops-tools/extra-content.css
      page_body: |
          ## Pricing Plans

          * Free ($0)
            - 2,500 free credits/week
            - Run 1 job at a time
            - Build on Linux or Window (no macOS support)
            - Does not support flexible payment option (credit card or invoice)
            - Support Options:
            - Community
          * Performance (starting at $30/month)
            - $15/month for the first 3 users and then $15/month for each added user
            - Starts at 25,000 credits for $15
            - Does not support flexible payment option (credit card or invoice)
            - Support Options:
              - Community
              - Support Portal
              - Global Ticket Support
              - 8×5 SLAs available
              - Account Team: Customer Success Manager
          * Custom
            - Plan customized for the customer
            - Support Options:
              - Community
              - Support Portal
              - Global Ticket Support
              - 24×5 and 24×7 SLAs available
              - Account Team: Customer Success Manager, Customer Success Engineer, Implementation Manager

  - path: key-differentiators
    content:
      page_title: CircleCI vs. GitLab Key Differentiators
      css: devops-tools/extra-content.css
      page_body: |
        # GitLab vs. CircleCI Analytics: Repo Insights

        ![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Repo-Insights.png)

        ## GitLab vs. CircleCI Analytics: Per Project Insights

        ![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Per-Project-Insights.png)

        ![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Per-Project-Insights2.png)

        ## CircleCI CI SaaS Analytics

        ![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/SaaS-Analytics.png)

        ## GitLab vs. CircleCI Dashboard: Viewing Pipelines

        ![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Viewing-Pipelines.png)

        ## GitLab vs. CircleCI Dashboard: Viewing Job Logs

        ![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Viewing-Job-Logs.png)

        ## GitLab vs. CircleCI Hybrid CI Orchestration

        ![GitLab CircleCI Comparison Chart](/devops-tools/circleci/images/Hybrid-CI.png)
