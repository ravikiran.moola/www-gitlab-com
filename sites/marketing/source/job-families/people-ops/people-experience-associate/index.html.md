---
layout: job_family_page
title: "People Experience"
---
<a id="associate-requirements"></a>

The People Experience Team which forms part of the broader [people group](https://about.gitlab.com/handbook/people-group/) is geared toward encouraging a positive and [value centric](https://about.gitlab.com/handbook/values/) encounter for all team members throughout their journey with GitLab.  Along with overseeing Onboarding; Career Mobility and Offboarding the People Experience Team supports various other functions and initiatves such as Employment Verifications; Probation Periods and the Anniversary Program.

## Responsibilities
- Ensure all GitLab team members experience extraordinary customer service when engaging with the People Experience Team either via Slack or Email - being mindful of the documented turnaround times within the People Experience Team SLA (this may from time to time include queries pertaining to specific entities and / or countries).
- Use the [Onboarding Satisfaction Survey](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) (OSAT) results as a guide for future iterations and improving the onboarding experience for new hires - individual feedback can be found in the private onboarding-survey slack channel.
- Ensure that the functions of the People Experience team are carried out with compliance in mind and always in complete alignemnt with GitLab policies.
- Assist in managing the GitLab Team Meetings Calendar, adding and updating events on request and ensuring scheduling conflicts do not occur.
- Announce all applicable People Experience related changes and improvements in the #whats-happening-at-gitlab Slack channel.
- Assist with and complete ad hoc projects, reporting and tasks as directed by the needs of the People Experience Team at any given time.
- Initiate the People Operations Disaster Recovery Plan in the event of a natural disaster or extreme weather event.
- Apply suitable handover measures to ensure a level of continuity in instances where you may be unable to attend to your regular tasks, particularly those that may have direct bearing on GitLab team members who may currently be Onboarding or Offboarding.

## Requirements
- Alignment to the GitLab values and a commitment to working in accordance with them at all times.
- Ability to use GitLab and work within it wherever possible.
- Able to work autonomously and self-drive performance and development.
- Previous experience in a people centric role placing a focus on Onboarding, Career Mobility, Transitioning and general People Operations.
- Willingness to work occasional odd hours.
- Exceptional team member engagement (customer service) skills.
- High calibre written and verbal communication capability.
- Collaborating across the team to extend support across a broad set of topics and tasks.
- Proven aptitude for learning and adopting new tools / technologies.
- Commitment to encouraging transparency within the team and the greater organisation.
- Proven attention to detail with exceptional organisational skills and able to prioritise tasks.
- Experience at a growth stage tech company.
- Able to work and cope in a fast paced role placing a sharp focus on accuracy and urgency.

##### Performance Indicators
- [Onboarding Satisfaction Survey > 4.5](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat)
- [Compliance Task Completion](https://about.gitlab.com/handbook/people-group/people-group-metrics/#compliance-task-completion)
- [Offboarding Task Completion](https://about.gitlab.com/handbook/people-group/people-group-metrics/#offboarding-task-completion)
- [Turnaround Time Alignment](https://about.gitlab.com/handbook/people-group/people-group-metrics/#lead-time-alignment)

## Levels

### People Experience Coordinator

#### Job Grade

The People Experience Coordinator is a [Grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
- Review onboarding team member contracts, ensuring that the entity is correct, the documenthas been signed by all pertinent parties and a PIAA is in place where applicable.
- Conduct regular audits of the referal bonuses applied for recent hires ensuring alignment to the guidelines set out but Total Rewards.
- Serve as first line of contact for all team member enquiries surrounding Anniversary Gifts via slack and / or email.
- Process requests for confirmation of employment in addition to reference, visa application and mortgage letters.
- Audit offboarding issues weekly, to ensure task completion in alignment with the documented five-day deadline.
- General business card and travel system administration.
- Onboarding Support i.e. invite new team members to the GitLab Team Meetings Calendar and the GitLab Unfiltered YouTube Channel; oversee the New Hire Swag process.
- Build understanding of GitLab employment practices and provide support as and when required.
- Administration (moderation) of company communication primarily Group Conversations (GC), the CEO 101 call and Ask Me Anything (AMA) sessions.
- Facilitate the ordering of Gifts and Flowers for significant life events on request, ensuring these are correctly expensed to the relevant team / department.
- Allocation of tasks to the People Experience Team to ensure even distribution of onboarding; offboarding; career mobility and rotational tasks.
- Re-allocation of tasks to ensure a level of continuity in instances where team members have indicated that they may be unable to attend to them by way of a handover discussion.

#### Requirements
- Bachelors Degree or two years of related experience.
- Proactive and result driven with strong attention to detail.
- Able to split focus across a diverse yet equally important set of functions.

### People Experience Associate

#### Job Grade
The People Experience Associate is a [Grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
- Administer and coordinate all Onboarding Tasks, maintaining contact with the onboarding team member in addition to other stakeholders such as the Manager; Onboarding Buddy and Tech Provisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Administer and coordinate all Offboarding Tasks, maintaining contact with the offboarding team member in addition to other stakeholders such as the Manager and Tech Deprovisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Administer and coordinate all Career Mobility Tasks i.e. promotions; demotions or lateral migrations, maintaining contact with the transitioning team member in addition to other stakeholders such as the Manager; Career Mobility Buddy (if one is assigned) and Tech Provisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Administer the Probation Period process i.e. liaising with Managers, communicating with People Business Partners and updating Team Members. This includes managing and updating team member probation period information in BambooHR.
- Support the Senior People Experience Associate with the administration surrounding Anniversary Gifts; New Hire Swag and other Gift Requests including where applicable mail merges; communication updates and responding to team member queries.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the team member and/or manager’s experience.
- Facilitate the ordering of Gifts and Flowers for significant life events on request, ensuring these are correctly expensed to the relevant team / department.
- Administration (moderation) of company communication primarily Group Conversations (GC), the CEO 101 call and Ask Me Anything (AMA) sessions.
- Foster collaborative working relationship with the CES team to ensure a new hire has a positive onboarding experience. Escalate any concerns to Senior People Experience Associate.
- Complete ad-hoc projects, reporting, and rotational tasks such as Employment Verifications, updating the GitLab Team Meetings Calendar etc.

#### Requirements
- Bachelors Degree or two years of related experience.
- Organized and efficient, able to develop, iterate, and execute against a plan.

### Senior People Experience Associate

#### Job Grade
The Senior People Experience Associate is a [Grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
- Monitor the rotation based tasks facilitated by the People Experience Team while ensuring even distribution of workload and that all tasks are closed out for each cycle i.e. weekly and monthly.
- Oversee the administrative processes surrounding Anniversary Gifts either directly or by way of a team rotation, ensuring emails are updated with the relevant links / codes and sent to all celebrants; making sure adequate stock is available within the warehouse and replenished after each cycle and responding to queries around shipping etc.
- Assist fellow People Experience team members when concerns or questions are raised, identifying suitable solutions and moving blocks or escalating when necessary.
- Continuously review feedback across all formal / informal communication mechanisms i.e. Emails; Slack Channels and Surveys (OSAT or otherwise) to identify trends and suitable opportunities for iteration and improvement of the issues and processes surrounding the People Experience Team.
- Report monthly on trends identified within the OSAT Survey results in addition to suggestions and feedback gathered.
- Administer and coordinate all Onboarding Tasks, maintaining contact with the onboarding team member in addition to other stakeholders such as the Manager; Onboarding Buddy and Tech Provisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Administer and coordinate all Offboarding Tasks, maintaining contact with the offboarding team member in addition to other stakeholders such as the Manager and Tech Deprovisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Administer and coordinate all Career Mobility Tasks i.e. promotions; demotions or lateral migrations, maintaining contact with the transitioning team member in addition to other stakeholders such as the Manager; Career Mobility Buddy (if one is assigned) and Tech Provisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Collaborate closely with the People Experience Associates and the People Experience Team Lead to ensure Onboarding, Offboarding and Career Mobility issues are able to scale with organisational growth and are continually updated to in alignment with the greater compliance and security requirements of the business.
- Foster partnered relationships with teams and individuals such as the Candidate Experience Specialists (CES) and People Business Partners (PBP) with the intention of supporting urgent / unanticipated Onboardings and Offboardings can be attended to seamlessly.
- Manage the GitLab Team Meetings Calendar either directly or indirectly by way of rotation based task.

##### Requirements
- Bachelors Degree or three years of related experience.
- Organized and efficient, able to develop, iterate, and execute against a plan.
- Able to give direction when necessary and serve as guide for other people experience team members.

### Team Lead, People Experience

#### Job Grade
The Team Lead, People Experience is a [Grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
- Coach and mentor the People Experience team to effectively represent GitLab's culture and values in all team member interactions.
- Embed a weekly People Experience Team meeting to calibrate on priorities; identify blocks and discuss projects underway.
- Continually audit and monitor compliance with GitLab policies and procedures applying recification measures and escalating to the Senior Manager, People Operations when necessary.
- Drive continued automation to enhance the team member experience and promote the GitLab efficiency value.
- Drive a positive team member encounter throughout their lifecycle with GitLab ensuring all touch points speak to the core company values.
- Continually audit and iterate on all offboarding processes to ensure they remain efficient, partnering with various teams to ensure team members are fully offboarded.
- Continually audit and iterate on all onboarding processes to ensure they remain impactful and efficient, partnering with various teams and hiring managers to ensure team members are fully onboarded.
- Continually audit and iterate on all career mobility processes to ensure they remain impactful and efficient, partnering with various teams to ensure team members are fully offboarded.
- Improve Career Mobility Issues and partner with Learning and Development to implement suitable training mechanisms with this in mind.
- Conduct a quarterly review of the Onboarding Satisfaction Survey and its results identifying opportunities to improve the onboarding experience.
- Support the People Experience Team on escalated queries / issues i.e. removing blocks and identifying suitable solutions.
- Complete ad-hoc projects, reporting, and tasks in alignment with the needs of the team and the greater People Group.

#### Requirements
- Bachelors Degree or three years of related experience.
- High calibre communication skills geared toward providing clear and concise feedback.
- Able to support over arching strategic objectives through tactical planning and execution.

### Manager, People Experience

### Job Grade
The Manager, People Experience is a [Grade 8](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Responsibilities
- Onboard and Coach the People Experience Team to effectively represent the GitLab values and culture through all interactions with GitLab Team Members and the broader GitLab Community.
- Encourage career development within the team through mentorship and coaching mechanisms while identifying suitable opportunities for fostering individual growth through stretch assignments, shadow programs, development initiatives etc.
- Establish consistent individual and team based engagement opportunities by way of 1:1s and Teach Catch-Ups to allow for alignment on strategic areas of focus for both the People Success Team and the greater People Group.
- Ensure that the outputs of the People Experience Team are conducive to creating a positive experience across the lifecycle of all GitLab team members.
- Engage with the Senior Director, People Success and Senior Manager, People Operations to identify upcoming areas of strategic direction, aligning on suitable people experience centric initiatives to support this.
- Gather data and insights that will support the development of team member engagement initiatives in alignment with the over arching strategies of the people group.
- Establish and nurture collaborative relationships with Managers and People Business Partners across all GitLab departments with the intention of ensuring all people experience touch-points i.e. Onboarding, Career Mobility and Offboarding are developed and iterated on to support the unique needs and evolving needs of each team.
- Seek out blockers to the people experience functions and outputs, identifying suitable solutions to encourage continued innovation and growth.
- Support the People Experience Team in effectively responding to team member queries, promoting at all times a handbook first approach.
- Promote compliance across all pertinent people experience tasks particularly in Alignment with GitLab policies and procedures in addition to federal requirements such as employment verifications etc. through continued auditing of outputs.
- Establish an SLA documenting suitable turnaround times for key people experience tasks / outputs and ensure these are adequately communicated, documented and adhered to in the interests of ensuring a phenomenal team member (customer) experience.
- Ongoing improvement of all people experience centric administration and functions in particular those surrounding Onboarding, Career Mobility and Offboarding.
- Review and report on the metrics surrounding the People Experience Team i.e. Onboarding Satisfaction (OSAT) and Onboarding Task Completion identifying trends and using them for improvements and iterations.
- Promoting efficiency often through automation to ensure that the processes surrounding the People Experience team are seamless and efficient.
- Collaborating with key stakeholders in the greater People Group to ensure all mechanisms utilised for Onboarding, Career Mobility and Offboarding are well leveraged to encourage team member success and self-enablement e.g. Learning and Development

#### Requirements
- Bachelors Degree or five years of related experience.
- Able to support over arching strategic objectives through tactical planning and execution.
- Able to gather and compile data-driven insights that can be used to support leadership decision making.
- Awareness of international data and labor practices to ensure compliance across core areas of accountability.

## Career Ladder
The next step in the People Experience career ladder would be to enter the [People Operations Specialist](https://about.gitlab.com/job-families/people-ops/people-operations/) job family.

## Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters.
- Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager.
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations Specialist and People Business Partner teams.
- After that, candidates will be invited to interview with the Senior Director of People Operations.
- Finally, our CPO may choose to conduct a final interview.

