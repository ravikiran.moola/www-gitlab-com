---
layout: job_family_page
title: "Technical Instructional Designer"
---

The Technical Instructional Designer position is a critical role within GitLab’s Field Enablement and/or Professional Services teams. The Technical Instructional Designer is responsible for designing and developing new, engaging, and effective learning solutions and instructional experiences for GitLab customers, partners, and/or sales & customer success professionals that help these audiences achieve desired learning outcomes. This role must be able to analyze, design, and develop blended, interactive, online and in-person learning solutions (with an emphasis on scalable online learning solutions) using sound instructional design principles. 

## Responsibilities

* Design and develop experiential learning solutions that support adult learning best practices, address needs identified in a job/task analysis, drive behavior change, and improve performance of GitLab customers, partners, and/or sales teams
* Follow GitLab branding guidelines and standards to ensure content is professional and consistent with corporate look and feel for external audiences
* Create and curate customer learning content using the latest technology and innovative techniques (especially via but not limited to scalable online delivery)
* Build and maintain valued relationships with key stakeholders and external vendors
* Design interactive courseware and video using adult learning theory that appeals to various learning styles and international audiences
* Utilize various modern authoring tools to rapidly build enjoyable, positive learning experiences that are interactive, effective and technically functional
* Develop instructionally sound knowledge, skill, and job performance-based assessments
* Evaluate the business impact of learning programs through the use of learning and performance metrics
* Gain a working knowledge of the various functional and technological product areas of GitLab
* Closely partner and collaborate with other members of the Field Enablement team to continuously improve existing learning programs based on the latest research in adult education
* Meet all milestones and final deliverables by deadlines; when delays do occur, work with the team to estimate, monitor, adjust, and proactively communicate revised deadlines as needed
* Be a mentor to sales enablement program managers, trainers, and business partners on best practices in instructional design and enablement solutions
* Take on additional projects and responsibilities as needed

## Requirements
* Bachelor’s degree required; Master’s Degree in Instructional Development, Training Education/Communication is preferred
* 5+ years related work experience in B2B SaaS customer, partner, and/or sales training instructional design (or equivalent combination of transferable experience and education)
* Deep experience designing and developing learning programs across delivery mediums and content types (mobile learning, e-learning, video, gamification, virtual learning, live simulation, and live classroom)
* Proven experience designing and developing scalable, engaging, online training is a must
* Demonstrated experience working with multiple industry recognized instructional design models preferred
* Must have the ability to work in an ever-changing, project-driven environment with a strong design sensibility when creating all customer, partner, and sales training and enablement instructional materials
* Strong oral and written communication skills and diverse experience working with internal customers and stakeholders in both live and virtual environments
* Proven ability to clearly articulate complex concepts in simple terms is essential
* Excellent team player
* Must be detail oriented and have strong project management skills needing minimal supervision
* Experience with a wide range of modern elearning authoring tools (e.g. recent versions of Adobe Captivate, Articulate 360 Suite, Go Animate, Elucidat, etc.)
* Working knowledge and application experience using reveal.js, Adobe Creative Suite (Premiere, Captivate, InDesign, Illustrator, Photoshop), and other industry benchmark applications and online multimedia authoring tools
* HTML5, xAPI, mobile learning, and adaptive learning experience desired
* Experience working with a variety of Learning Management and Learning Experience Systems and various virtual meeting management tools such as Zoom is a plus
* Familiarity with statically generated websites are preferred, as we take a [handbook-first](/handbook/handbook-usage/#why-handbook-first) approach to Field and Customer Enablement
* Knowledge of the software development life cycle, DevOps, and/or open source software is preferred
* Ability to use GitLab

## Levels

### Technical Instructional Designer (Intermediate)

#### Job Grade 

The Technical Instructional Designer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Successfully accomplishes responsibilities with minimal direction or oversight

#### Requirements

* Ability to consistently meet deadlines and execute on projects with positive feedback from stakeholders

### Senior Technical Instructional Designer

#### Job Grade 

The Senior Technical Instructional Designer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Works quickly and iteratively on projects with very limited direction from others
* Proven experience designing engaging and effective learning solutions for B2B enterprise sales professionals
* Viewed as a subject matter expert and trusted advisor by key stakeholders
* Innovates and continually improves the delivery of world-class learning solutions
* Regularly shares best practices and provides constructive coaching and feedback to other members of the team

#### Requirements

* 5+ years experience in an instructional designer role, preferably with expertise in DevOps and/or Open Source
* Self-manages workload to meet deadlines and prioritize accordingly

## Performance Indicators
* Learner satisfaction scores
* Number and percentage of on-time, on-budget learning solutions delivered
* Additional performance indicators to be determined

## Specialties

### Field Enablement
* Focus on design of impactful training and enablement solutions for GitLab sales professionals (Enterprise and Public Sector Strategic Account Leaders, Mid-Market Account Executives, SMB Customer Advocates, and Inside Sales Reps) across the sales professional lifecycle and across all routes to market

### Customer Education
* Focus on design of impactful training and enablement solutions for GitLab customers

## Career Ladder

The next steps for the Technical Instructional Designer Job Family would be to move to the [Sales Training Facilitator, Sales and Customer Enablement](/job-families/sales/sales-training-facilitator-field-enablement/) Job Family or the [Program Manager, Field Enablement](/job-families/sales/program-manager-field-enablement/) Job Family.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Videoconference (or phone) screen interview with a GitLab Talent Acquisition team member
* Videoconference interviews with 2-3 additional GitLab team members
* Final videoconference interviews with the Director, Field Enablement and/or VP, Field Operations
Additional details about our process can be found on our [hiring page](/handbook/hiring).
