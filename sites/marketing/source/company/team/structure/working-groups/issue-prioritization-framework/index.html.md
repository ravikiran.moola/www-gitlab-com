---
layout: markdown_page
title: "Issue Prioritization Framework"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | August 6, 2020 |
| Target End Date | November 6, 2020 |
| Slack           | [#wg_issue-prioritization](https://join.slack.com/share/zt-etotbmm9-FzhcHH0BGbw3~D4Xe5rAyg) (only accessible from within the company) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1oBWNxBSOJKrh3ubHwN5pI8243vBjJ-Y_Cax17A5abII/edit) (only accessible from within the company) |
| Docs      | TBD |
| Related Issue   | [Adding context to customer's requests](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/907) |
| Associated OKRs | [Increase TMAU and Paid TMAU](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team/-/issues/80) |

## Problem To Solve

- We currently do not distinguish between "nice to have", "blocker", and "this will likely cause a downgrade/churn/prevent upsell." One customer may view Feature ABC as nice to have, whereas another customer desperately needs it.unt of time necessary to collect this information, transfer it, and then disseminate it on the other side is likely to be inefficient and error prone. It also does not solve the problem of a standardized model for determining relative priority across all stages based on quantifiable data such as impact to IACV 
- We are [currently proposing](https://gitlab.com/gitlab-com/customer-success/okrs/-/issues/23) that we establish a highly manual process where individuals will act as bridges to transfer information across departments. The amoor ARR.
- We are not leveraging our existing data to the fullest extent. Let's understand why and solve for this. 

## Business Goal

- Create a standardized prioritization framework based on quantifiable data that enables us to determine urgency/value per issue/epic at scale and speed so that Product, Sales, and Customer Success can use a common language and model when discussing prioritization trade-offs.
- Product DRIs have more accurate inputs to help them globally optimize their delivery backlogs resulting in increased customer retention and acquisition.
- Positively impact IACV growth and improve retention of existing ARR.
- Improve the accuracy of the prioritization feedback among departments.
- Further operationalize the process of creating effective bridges between Customer Success, Sales, and Product.

### Exit Criteria

 (✅ Done, ✏️ In-progress)

##### Step 1: [Determine a viable model (TBD)]() `=> 0%`

- TBD (Working Group will determine during first meeting)
- TL;DR: We look at all the existing prioritization models out there and select one that we determine best aligns with our business.

##### Step 2: [Implement & validate model (TBD)]() `=> 0%`

- TBD (Working Group will determine during first meeting)
- TL;DR: We do the necessary work to implement this -- both the process and the data collection -- and validate that it works well. 

##### Step 3: [Verify at scale and measure outcomes (TBD)]() `=> 0%`

- TBD (Working Group will determine during first meeting)
- TL;DR: Once validated, we drive adoption across Product/CS/Sales and collect measurements to confirm that this model is producing the desired business outcomes. If not, decide to go back to Step 1 or Exit.

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | David Sakamoto           | VP Customer Success         |
| Facilitator           | Gabe Weaver              | Senior Product Manager, Plan    |
| Functional Lead       | TBD (Data)               | TBD |
| Functional Lead       | Jeff Beaumont            | Senior Manager, CS Ops |
| Functional Lead       | TBD (Sales)              | TBD |
| Member                | Patrick Harlan           | Manager, Technical Account Managers (Commercial) |
| Member                | Sophie Pouliquen         | Senior Techical Account Manager |
| Member                | Nick Post                | Senior Product Designer, Interim Product Manager (Manage: Analytics) |
| Member                | Mek Stittri              | Director of Quality Engineering |
| Member                | Kyle Wiebers             | Engineering Manager, Engineering Productivity |
| Member                | Farnoosh Seifoddini      | Product Operations |
| Member                | Jonathan Fullam          | Solutions Architecture |
| Member                | Sherrod Patching         | Director, Technical Account Managers |

## Meetings

Meetings are recorded and available on
YouTube in the [Working Group - Issue Prioritization Framework](https://www.youtube.com/playlist?list=PL05JrBw4t0KrKoeXjf5Bdtapu9Cl3T7gI) playlist. Due to the subject matter of this working group and the high probability that every syncronous meeting will discuss sensitive customer information, the playlist is private and accessible by GitLab team members only.
