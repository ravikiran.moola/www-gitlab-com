---
layout: handbook-page-toc
title: "The GitLab Legal Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
# Welcome to GitLab Legal!

We are glad you are here! Meet our [team](https://about.gitlab.com/company/team/?department=legal)

Please check out this [tutorial video](https://www.youtube.com/watch?v=Kh_pPeJkF2o) on the best way to reach Legal, if you ever have any questions you can always reach out on Slack.

## Contacting the Legal Team with General Legal Questions

### 1. Quick Questions

You can reach out to the Legal Team on the *[`#legal`](https://gitlab.slack.com/archives/legal)* Slack chat channel.  The legal Slack chat channel is reserved for everyday legal questions that can be answered in informal communication. It is not for requests that require legal advice, deliverables, discussion of confidential information.

**Please do not share confidential information on Slack that is not meant for the entire company to see, and do not use it to seek legal advice.**

### 2. Sales Guide: Collaborating with GitLab legal 
**Are you a member of GitLab Sales?** Please find additional information related to engaging legal within the [Sales Guide: Collaborating with GitLab Legal](https://about.gitlab.com/handbook/legal/customer-negotiations/)


### 3. Requests with Deliverables
If you are making a request that requires some sort of deliverable, please use the list below to determine how you should reach out. If you are unsure where your non-Slack request fits, refer to [no. 6 below](https://about.gitlab.com/handbook/legal/#6-other-legal-requests). 

## Requesting Legal Services
 
How do I request the services I need?

### 1. Internal Ethics and Compliance Reporting (Anonymous)

We take employee concerns very seriously and encourage all GitLab team members to report any ethics and/or compliance violations by using Lighthouse. Futher details are found on the [People Ops Handbook page](https://about.gitlab.com/handbook/people-group/code-of-conduct/#how-to-contact-gitlabs-24-hour-hotline).

### 2. Privileged / Confidential Communications
If you have a request that involves confidential and/or sensitive information, including related to other GitLab team members, please e-mail legal@gitlab.com.  

For more information on Attorney-Client Privilege, see the [General Topics and FAQs](https://about.gitlab.com/handbook/legal/#general-topics-and-faqs) below. 

### 3. Vendor, Partner, and other "To Pay" Contracts
* If you are looking for a new vendor, need an NDA for a vendor or partner, or need review of a vendor or partner contract, these services are handled by the Procurement Team. For purposes of this process, anyone that will receive payment from GitLab is considered a vendor. 
* Legal will be brought in by Procurement for escalations only. Please see the [Procurement Page](https://about.gitlab.com/handbook/finance/procurement/) for more information on the Vendor Management Process. Once a Vendor NDA and/or Contract has been completed, it should be uploaded by the requestor into our contract management database tool [ContractWorks](https://about.gitlab.com/handbook/legal/vendor-contract-filing-process/). If you need a license, you will need to submit an Access Request.

### 4. Requests for Insurance Certificate
* If you need an insurance certificate (other than for worker's compensation) you can send an email request directly to our insurance broker at *[ABD](mailto:abdcertdept@theabdteam.com).*    You will need to include contact information for the customer seeking to be added to the certificate and any other specific requirements relating to the coverage. If you require an insurance certificate for worker's compensation email: payroll@gitlab.com with the same information.

* For a [summary of GitLab's insurance coverage](https://drive.google.com/file/d/17pvhu9cKtoQatO7xOlKbwSoOXDEJRpiQ/view?usp=sharing) please refer to this link.

### 5. Other Legal Requests 

* For requests that are not Customer related, but require input from the Legal team or a deliverable such as assistance with questionnaires, engineering license agreements, open source question, internal operation matters, or compliance questions, please tag the relevant Legal team member and ask your questions in the issue that relates to your question. If there is not an issue related to your question, please create an issue in the [Legal and Compliance Issue Tracker](https://gitlab.com/gitlab-com/legal-and-compliance/issues) using the issue template. 
**All Legal and Compliance issues should be marked as confidential. GitLab team members will be able to access these issues directly.**

Please be sure to include sufficient detail regarding your request, including time-sensitive deadlines, relevant documents, and background information necessary to respond.


 
# Contract Templates
 
- [Mutual Non-Disclosure Agreement](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)

# General Topics and FAQs

## 1. The Attorney-Client Relationship in the United States

This discussion is limited to U.S. practices because currently our team members only communicate with U.S. practicing attorneys.  As we continue to grow globally we will update this and expand how privilege applies in other jurisdictions.

### What is the Attorney-Client Privilege?

Attorney-Client Privilege is a law that has been adopted in each of the states of the U.S in some form.  Generally, the law protects communications between clients and their attorneys for the purpose of seeking legal guidance and advice.  The information is not protected if it is available from another source.  Therefore, information cannot be placed under the protections of Attorney-Client privilege simply by telling your attorney or copying your attorney on a communication.  In addition, the underlying facts are also not protected, only the opinions and analysis of the facts, and discussions thereof, with the attorney.  The privilege belongs to the client, and therefore, can only be waived by the client.

### What is Work Product?

Work Product is a U.S. doctrine in which an attorney’s notes, observations, thoughts, and research prepared by, or at the direction of an attorney, in anticipation of litigation, are protected from being discoverable during the litigation process.

### What is the purpose of these Privileges?

The purpose of the Attorney-Client and Work Product privileges is to allow clients to speak freely with their attorneys and encourage full disclosure so they can receive accurate and competent legal advice without the fear of having their attorney compelled to testify against them and disclose the information shared by the client.

### Who do these Privileges Apply to at GitLab?

There is not one uniform answer that covers all jurisdictions in the U.S. 

A minority number of states apply the **Corporate Group Test**. This test is quite restrictive and only allows for the protection of corporate communications to the corporation's controlling executives and managers.

A more commonly used test is the **Subject Matter Test**.  Instead of looking at the roles of the employees involved, this test looks at the subject matter of the employees’ communications. The test will look to see if the employee was instructed to discuss the subject matter with the attorney should be protected and if the subject matter of that communication relates to the performance by the employee of the duties of his or her employment.

A slightly modified version of the **Subject Matter Test** called the **Upjohn Test** is also widely used. Under the **Upjohn Test** the privilege is applied only if the following criteria are satisfied:

* The employee must have made the communication to counsel for the purpose of seeking legal advice regarding the corporation.
* The substance of the communication must involve matters that fall within the scope of the employee's job duties.
* The employee must be aware that the statements are being provided for the purpose of obtaining legal advice for the corporation.
* The communications also must be confidential when made and must be kept confidential by the company.

The Supreme Court case which established the  **Upjohn Test** is also important because it resulted in the **_Upjohn Warning_** which is a procedure in which a company’s attorney explains that he or she does not represent the employee individually, but instead represents the interests of the company.  This is important to note because a company can waive its privilege at any time, meaning the company could choose to disclose information the attorney received from a covered employee in confidence for use as evidence in a legal proceeding in order to protect the company from liability.
 
 The **Subject Matter Test** and **Upjohn Test** are the most commonly used tests. More information about the tests can be found [HERE](https://drive.google.com/open?id=1Rzm_oFo-nGx8RWZgSaZBOCPBL1kCrce3)
 
### How do you Protect the Privileges that Apply to You When Seeking Legal Advice?
 
 * Direct the communication to a practicing licensed attorney.  Privilege does not apply to other non-attorney members of the legal team.
 * It is best practice to have privileged conversations with the attorney via Zoom.
 * If other individuals will need to participate in the discussion, consult with the attorney, and only include the minimum necessary individuals in the conversation, in other words, keep the circle of trust small.
 * If it is necessary to communicate by email, in the “Subject” line, and at the top of the body of the communication, include the phrase **“AC PRIV”**.
 * Do not overuse the claim of privilege. Limit its use to when actually seeking legal guidance and advice and not on any and every correspondence with the attorney.
 * You must keep the information discussed confidential, and not share it with anyone outside the circle of trust without first consulting with the attorney.
 * Do not forward protected emails to anyone outside the circle of trust.
 * Do not copy anyone outside the circle of trust on emails with the attorney.
 * As with anything else, if you ever have any questions or concerns, do not hesitate to reach out to the Legal team.

## 2. Litigation Holds

### What is a Litigation Hold?

A litigation hold is the process a company uses to preserve all forms of relevant evidence, whether it be emails, instant messages, physical documents, handwritten or typed notes, voicemails, raw data, backup tapes, and any other type of information that could be relevant to pending or imminent litigation or when litigation is reasonably anticipated.  Litigation holds are imperative in preventing spoliation (destruction, deletion, or alteration) of evidence which can have a severely negative impact on the company's case, including leading to sanctions.

Once the company becomes aware of potential litigation, the company's attorney will provide notice to the impacted employees, instructing them not to delete or destroy any information relating to the subject matter of the litigation.  The litigation hold applies to paper and electronic documents. During a litigation hold, all retention policies must be overridden.

## 3. Human Rights

### How is GitLab advancing Human Rights?

We are committed to upholding fundamental human rights and believe that all human beings around the world should be treated with dignity, fairness, and respect. Our company will only engage suppliers and direct contractors who demonstrate a serious commitment to the health and safety of their workers, and operate in compliance with human rights laws. GitLab does not use or condone the use of slave labor or human trafficking, denounces any degrading treatment of individuals or unsafe working condition, and supports our products being free of conflict minerals.

#### Anti-Slavery and Anti-Human Trafficking Policy

1. Slavery and Human Trafficking are crimes and violations of fundamental human rights. These violations take various forms, such as slavery, servitude, forced and compulsory labour, and/or human trafficking, all of which have in common the deprivation of a person’s liberty by another in order to exploit them for personal or commercial gain. GitLab is committed to acting ethically and with integrity in our business dealings and relationships by implementing and enforcing systems/controls to ensure modern slavery or human trafficking are not taking place in our business, or with those with whom we do business.

2. GitLab is also committed to ensuring there is transparency in our business and in our approach to tackling slavery and human trafficking throughout our supply chains and overall organization, consistent with disclosure obligations we may have under applicable law. To that end, we prohibit the use of forced, compulsory or trafficked labor, or anyone held in slavery or servitude, whether adults or children by anyone working for or with GitLab.

3. All employees, directors, officers, agents, interns, vendors, distributors, resellers, contractors, external consultants, third-party representatives and business partners are expected to comply with this policy.

4. Every Team Member is responsible to assist in the prevention, detection and reporting of slavery and human trafficking by those working for or with GitLab.  Each Team Member is encouraged to raise concerns about any known or suspected incidents of slavery or human trafficking in any parts of our business or supply chains at the earliest possible stage.   If you are unsure about whether a particular act, the treatment of workers more generally, or their working conditions within any tier of our supply chains or business partners constitutes any of the various forms of modern slavery/human trafficking, raise it at Compliance@Gitlab.com.

5. We may terminate our relationship with individuals and/or Business Partners if they breach this policy.

# Important Pages Related to Legal
 
* [Compliance](https://about.gitlab.com/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [General Guidelines](https://about.gitlab.com/handbook/general-guidelines/) - general company guidelines
* [Terms](https://about.gitlab.com/terms/) - legal terms governing the use of GitLab's website, products, and services
* [Privacy Policy](https://about.gitlab.com/privacy/) - GitLab's policy for how it handles personal information
* [Employee Privacy Policy](https://about.gitlab.com/handbook/legal/employee-privacy-policy/) GitLab's policy for how it handles personal information about team members.
* [Trademark](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/#trademark) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents
* [Requirements for Closing Deals](https://about.gitlab.com/handbook/business-ops/order-processing/#step-7--submitting-an-opportunity-for-approval) - requirements for closing sales deals
* [Acceptable Licenses](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/licensing.md) - list of acceptable licenses covering third party components used in development
* [Records Retention Policy](https://about.gitlab.com/handbook/legal/record-retention-policy/) - GitLab's policy on the implementation of procedures, best practices, and tools to promote consistent life cycle management of GitLab records


## Legal Processes
 
- [Uploading Third Party Contracts to ContractWorks](https://about.gitlab.com/handbook/legal/vendor-contract-filing-process/)
- [Issue Tracker Workflows](https://about.gitlab.com/handbook/legal/Issue-Tracker-Workflows/)

## Performance Indicators

### Administer, maintain, and manage the legal issue tracker (daily, ongoing) <= 24 hours
Triage and assign issues in the legal issue tracker to the appropriate legal team member within 24 hours of receipt, during regular business hours

### All suspicious transactions are cleared, actioned, or escalated  <= 1 Business Day
All suspicious transactions are cleared, actioned or escalated within 1 business day. This is tracked in Visual Compliance. 

### Annually review policies to ensure compliance with applicable laws,  update accordingly and communicate with business = 100% 
Over a rolling 12 months all policies to be reviewed and updated to be within compliance as documented on the Compliance Strategy Overview for the particular time period.  All updates are communicated with business. 

### Audit Open Source License compliance with policy = 100%
On a monthly basis audit all new open source licenses to ensure compliance with the policy after each release. 
Ensure proper license types are being used

### Ensure all federal government submissions, representations, and certifications are audited and accurate = 100%  
Verify all federal government submissions, representations and certifications are reviewed and accurate.

### Ensure all fully executed vendor contracts are in ContractWorks = 100%
Administer, maintain, and manage ContractWorks by ensuring all fully executed vendor contracts are uploaded with terms, and that all fields are complete. This will be measured on a monthly basis and the target is 100%.

### Ensure all fully executed sales contracts are in the Salesforce = 100%
Administer, maintain, and manage Salesforce by ensuring all fully executed sales contracts are uploaded with terms, and that all fields are complete. This will be measured on a monthly basis and the target is 100%.

### Ensure protections over corporate trademarks = 100%
File annual registrations and respond to challenges to intellectual property rights throughout the year based on registration dates of trademarks. This is tracked in Marcaria

### Negotiation Cycle Average Days per Quarter <= 90 days
Average number of days on a quarterly basis in “Negotiating” of 90 days or less. This is contingent upon the updated SFDC Legal operations model. There will be a report that shows when a contract negotiation begins, and when it is closed.  

### Number of Opportunities closed by Contract Manager(s) per Quarter >= 66
The average number of Opportunities (with contracting needs) closed per quarter to be equal or greater than 66, with annual total of 264 per Contract Manager. This is contingent upon the number of contracts brought forward by the sales team. 

### Percentage of contract negotiations per quarter <=15% 
This is calculated by taking the number of opportunities closed per quarter by Contracts Managers divided by the total number of opportunities closed per quarter. In the future this will be tracked in Salesforce. 

### Response times to initial requests for review <= 24 business hours
Monthly average response time within 24 business hours in the future this will be tracked in Salesforce for all Contract Managers. 

### ‘Turn-Around’ times on received red-lines <= 72 business hours
Monthly average red-lines / legal answer(s) within 72 business hours in the future this will be tracked in Salesforce for all Contract Managers. This is contingent on type of Agreement (MSA vs. NDA).
 
### Vendors and applicable commercial partners agree to Partner Code of Ethics = 100%
Strive for 100% compliance on vendors and applicable commercial partners agreeing to Partner Code of Ethics. This will be audited by Internal Audit.





