---
layout: handbook-page-toc
title: "Analyzing and synthesizing user research data"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


### Analyzing and synthesizing user interview data

It can be tempting to complete a set of interviews or usability tests and think your research project is done. But without a process for systematically reviewing and making sense of the data you've collected, you risk leaving valuable insights on the table. The next step is to zoom out and look at your findings anew through a process called **research synthesis.**

Synthesis helps us take all of the info we've collected, organize it into patterns and themes, and translate those themes into actionable insights. At the end of this process, you should have answers to the core research questions you set out to answer and evidence that proves or disproves your [hypotheses](/handbook/engineering/ux/ux-research-training/defining-goals-objectives-and-hypotheses/).

Below are some guidelines for how to set yourself up for successful analysis and synthesis before you begin talking with users, while you're conducting research sessions, and after your interviews are complete.

#### Before interviews: Decide on a collection method

Conducting user research can result in a pretty hefty amount of information. To stay organized throughout this process, spend some time up front deciding where you'll collect your notes. Collect all your research notes for a study into a single Google spreadsheet. Doing this makes it easier for your team to collaborate by adding their own notes and observations, and collecting everything in a central place will keep things organized and consistent.

Here are some note taking resources you can use, depending on the type of study you're conducting:

- [A guide to user research note taking](https://condens.io/user-research-note-taking/) This guide shows how to take good notes to save time later on. It also compares alternatives to note taking in a spreadsheet e.g. using post-its. 
- [User Interview Note Taking Template](https://docs.google.com/spreadsheets/d/1hnIqg-fnCYW2XKHR8RBsO3cYLSMEZy2xUKmbiUluAY0/edit#gid=0)
- [Usability Testing Rainbow Analysis Chart](https://docs.google.com/spreadsheets/d/1bPg6op9Sk46lFVGaET-fruE0qz-ctNQsxbZKF-5lpn4/edit#gid=0). This approach uses a templated and color-coded spreadsheet to record what participants did during the test. For a thorough walkthrough on how to use this method, check out [this article](https://userresearch.blog.gov.uk/2019/09/13/how-a-spreadsheet-can-make-usability-analysis-faster-and-easier/), or watch [this video of a GitLab researcher's experience using this method](https://drive.google.com/file/d/1fYRTmaHZjMwDQfAnVpaEqHP1dByy1X5x/view?usp=sharing) (starts at 7:00).

#### During interviews: Gather data and discuss with your team

While capturing your research data, you'll focus on documenting **observations** (what you saw the user do, or what problems you saw/heard them experience) and **quotes** (the verbatim of what the user actually said, in quotation marks). You may also want to make notes on early **interpretations** (what you believe something a user said or did means) and possible **solutions** (concrete ways to solve the problems identified).

While the bulk of research synthesis happens after you've finished gathering all your data, analysis can start right away. After every user interview, de-brief with your teammates who observed the session about what stood out.

Capturing these insights while the session is fresh in your minds makes the overall process much faster and easier. Discussing observations as a group also leads to more thoughtful analysis, reduces [cognitive bias](https://medium.com/better-humans/cognitive-bias-cheat-sheet-55a472476b18), and helps your team form a strong shared understanding of the problem space you're investigating.

#### After interviews: Find patterns and themes

**Synthesizing user interview data**

Affinity diagramming is a way of finding themes in a collection of ideas, quotes, or observations. This method helps you draw out insights from qualitative data quickly and collaboratively. This is traditionally done in person with Post-It notes on a large blank wall or whiteboard. At GitLab, we use a tool called [MURAL](https://mural.co/) to recreate this experience remotely.

**1. Prep your notes**

Spend some time cleaning up and organizing your notes before moving them into MURAL. How you organize your notes in MURAL is up to you and your project goals. Some teams will start by organizing their notes by user, or by interview questions.

Next, copy and paste your notes into MURAL. Each cell in your notes spreadsheet should paste into MURAL as a separate sticky note.

**2. Cluster the data into themes**

Once all your individual data points are in MURAL, begin by grouping similar stickies together. You may want to start by grouping stickies for themes that stood out to you from your post-session debriefs, then move to surfacing themes that may not have been as obvious the first time around.

Here are some potential ways to group findings:

- **Equivalence:** e.g., "This finding is the same as this other finding."
- **Association:** Around the same area of the experience being analyzed, e.g., "This finding is best considered at the same time as this other finding."
- **Hierarchy:** Larger thematic trends which several findings support, e.g., "This finding is an example of this other finding."

This process might feel messy, especially if it's your first time doing an affinity diagram. To add more structure to the process, use your original project goals, hypotheses, and research questions as a guide during synthesis. You can copy these right into your MURAL board so you can keep them top-of-mind while you group individual findings into broader themes. You might also reference the [questions listed in this article](https://www.smashingmagazine.com/2013/09/5-step-process-conducting-user-research/#5-synthesis-answer-our-research-questions-and-prove-or-disprove-our-hypotheses) to ask yourself and your team throughout the synthesis process.

As your groupings start to come together, use a text heading to write titles or catch phrases that summarize each cluster of similar stickies.

**3. Discuss and revise as needed**

Organize and reorganize your findings into meaningful categories until everyone seems to be in agreement. See if you need to adjust any of your themes before moving onto the next step. 

This is also a good opportunity to check for possible bias. Try “[arguing the other side](https://uxdesign.cc/how-to-look-at-evidence-and-not-translate-it-into-your-own-agenda-9860171b7ba9).” In other words, building a case—from your research—*against* your key insights, to see if they still stand up.

**4. Distill findings into insights**

Once you're happy with your groupings, distill your findings into a manageable number of [insight statements](http://www.designkit.org/methods/62). Insights you uncover should come from multiple sources in your research. Depending on how much research you did, the number of insights you uncover may vary.